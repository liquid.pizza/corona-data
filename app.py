from dash import Dash, html
import corona_data
from views import gen_date_picker, gen_grouped_bar, gen_incidence_div
from dash.dependencies import Input, Output
from datetime import datetime as dt

app = Dash(
    __name__,
    external_stylesheets=['https://codepen.io/chriddyp/pen/bWLwgP.css'],
    )

def visualize_data():

    return html.Div(children=[
        html.H1(children='Welcome to Corona stats'),
        gen_date_picker(),
        html.Div(id="content_div"),
    ])

@app.callback(
    Output("content_div", "children"),
    Input("date_picker", "date"),
)
def gen_content(date):
    date = dt.strptime(date.split("T")[0], "%Y-%m-%d")
    print(f"date is {date}, {type(date)}")
    # if date is None:
    #     return

    df = corona_data.load_df((dt.today()-date).days)

    return html.Div([
        html.Div(children=f"Last updated {dt.now()}"),
        html.Div(children=[
                gen_incidence_div(df),
                gen_grouped_bar(df),
            ],
        ),
    ])

if __name__ == "__main__":
    app.layout = visualize_data
    app.run_server(host="0.0.0.0", debug=True)