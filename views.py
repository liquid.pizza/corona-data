from dash import html, dcc
import corona_data
import plotly.graph_objects as go
import plotly.express as px
from datetime import datetime as dt, timedelta

def gen_current_day(fig):
    return dcc.Graph(
        id="current_day",
        figure=fig,
    )

def gen_colored_bar(fig):
    return dcc.Graph(
        id='colored_bar',
        figure=fig,
    )

def gen_incidence_div(df):

    fig_colored_bar = px.bar(df, x=corona_data.DATE_LABEL, y=corona_data.INCIDENCE_LABEL, color=corona_data.INCIDENCE_CHANGE_LABEL, color_continuous_scale="bluered")

    fig_current_day = go.Figure(
        go.Indicator(
            mode="number+delta",
            value=df[corona_data.INCIDENCE_LABEL].iloc[-1],
            delta={"position": "top", "reference": df[corona_data.INCIDENCE_LABEL].iloc[-2]},
        )
    )

    return html.Div([
        html.Div([
                gen_current_day(fig_current_day),
            ],
            className="two columns",
        ),
        html.Div([
                gen_colored_bar(fig_colored_bar),
            ],
            className="ten columns",
        ),
    ],
    className="row")

def gen_grouped_bar(df):

    fig_grouped_bar = go.Figure(data=[
        go.Bar(name="1. Vaccinations", x=df[corona_data.DATE_LABEL], y=df[corona_data.FIRST_VACC_LABEL]),
        go.Bar(name="2. Vaccinations", x=df[corona_data.DATE_LABEL], y=df[corona_data.SECOND_VACC_LABEL]),
        # go.Bar(name="Booster Vaccinations", x=df[corona_data.DATE_LABEL], y=df[corona_data.BOOSTER_VACC_LABEL]),
    ])

    return html.Div([
        dcc.Graph(
            id='grouped_bar',
            figure=fig_grouped_bar,
        )
    ],
    className="row")

def gen_date_picker():
    return dcc.DatePickerSingle(
        id='date_picker',
        date=dt.today()-timedelta(30),
        max_date_allowed=dt.today()-timedelta(3),
        first_day_of_week=1,
        display_format="DD. MMM Y"
    )