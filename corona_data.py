import pandas as pd
import requests

NUM_DAYS = 30

DATE_LABEL = "date"
INCIDENCE_LABEL = "weekIncidence"
INCIDENCE_CHANGE_LABEL = "incidenceChange"
FIRST_VACC_LABEL = "firstVaccination"
SECOND_VACC_LABEL = "secondVaccination"
BOOSTER_VACC_LABEL = "boosterVaccination"

DATE_FORMAT_STRING = "%d.%m.%y"

def load_df(num_days=NUM_DAYS):

    res = requests.get(f"https://api.corona-zahlen.org/germany/history/incidence/{num_days}")

    df_incidence = pd.DataFrame.from_dict(res.json()["data"])
    df_incidence[DATE_LABEL] = pd.to_datetime(df_incidence[DATE_LABEL])

    res = requests.get(f"https://api.corona-zahlen.org/vaccinations/history/{num_days}")

    df_vaccinations = pd.DataFrame.from_dict(res.json()["data"]["history"])
    df_vaccinations[DATE_LABEL] = pd.to_datetime(df_vaccinations[DATE_LABEL])

    df = pd.merge(df_incidence, df_vaccinations)

    df[INCIDENCE_CHANGE_LABEL] = df[INCIDENCE_LABEL] - df[INCIDENCE_LABEL].shift(1)
    df[INCIDENCE_CHANGE_LABEL].fillna(0, inplace=True)

    df[DATE_LABEL] = df[DATE_LABEL].dt.strftime(DATE_FORMAT_STRING)

    # print(df)

    return df

if __name__ == "__main__":
    load_df()